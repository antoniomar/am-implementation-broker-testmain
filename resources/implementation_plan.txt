{
  "plan_id": "1154982",
  "plan_act_id": "",
  "sla_id": "125",
  "supply_chain_id": "",
  "creation_time": "",
  "monitoring_core_ip": "192.168.1.150",
  "monitoring_core_port": "5000",
  "iaas": {
    "provider": "OpenStack",
    "zone": "RegionOne",
    "appliance": "RegionOne/742feed8-d98b-4193-8603-33238c012803",
    "hardware": "RegionOne/3"
  },
  "pools": [
    {
      "pool_name": "",
      "pool_seq_num": "",
      "nodes_access_credentials_reference": "",
      "vms": [
        {
          "vm_seq_num": "",
          "public_ip": "$$ filled by Implementation",
          "components": [
            {
              "component_id": "nginx-node",
              "cookbook": "tomcat7",
              "recipe": "default",
              "implementation_step": "1",
              "acquire_public_ip": "false",
              "private_ips_count": "1",
              "firewall": {
                "incoming": {
                  "source_ips": [
                    ""
                  ],
                  "source_nodes": [
                    "ha-proxy"
                  ],
                  "interface": "private:1",
                  "proto": [
                    "TCP"
                  ],
                  "port_list": [
                    "22",
                    "80",
                    "443"
                  ]
                },
                "outcoming": {
                  "destination_ips": [
                    ""
                  ],
                  "destination_nodes": [
                    "ha-proxy"
                  ],
                  "interface": "private:1",
                  "proto": [
                    "TCP"
                  ],
                  "port_list": [
                    "*"
                  ]
                }
              },
              "private_ips": [
                "172.31.48.150"
              ]
            }
          ]
        }
      ]
    }
  ],
  "slos": [
    {
      "slo_id": "",
      "capability": "",
      "metric_id": "",
      "unit_type": "",
      "unit": "",
      "value": "",
      "operator": "",
      "value1": "",
      "value2": "",
      "interval_step": "",
      "importance_level": "LOW"
    }
  ],
  "measurements": [
    {
      "msr_id": "",
      "msr_description": "",
      "frequency": "",
      "metrics": [
        ""
      ],
      "monitoring_event": {
        "event_id": "",
        "event_description": "",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "",
          "threshold": ""
        }
      }
    }
  ]
}