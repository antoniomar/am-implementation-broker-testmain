/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package antomar.implementationtest;

import java.util.HashMap;
import java.util.Map;

import eu.specs.datamodel.broker.ProviderCredential;

public class ProviderCredentialsManager {
	
	private static Map <String, ProviderCredential> map = new HashMap<String, ProviderCredential>();


	public static void add(String provider, ProviderCredential cred){
			map.put(provider, cred);	
	}
	
	 
	public static ProviderCredential getCredentials(String provider){
		return map.get(provider);
	}
	
}
