package antomar.implementationtest;

public class CustomScripts {

	/**
	 * This method returns the script usefull to get the secret code from a chefServer Vm
	 * @return the script usefull to get the secret code from a chefServer Vm
	 */
	public static String[] getSecretCodeFromChefServer(){
		String[] script= {"cat /mos/etc/mos/secret.json"};
		return script;
	}
	
	
}
