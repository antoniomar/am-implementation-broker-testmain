package antomar.implementationtest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.UUID;

import org.jclouds.rest.ResourceNotFoundException;

import eu.specs.datamodel.broker.NodeCredential;
import eu.specs.datamodel.broker.NodeCredentialsManager;
import eu.specs.datamodel.broker.ProviderCredential;
import eu.specs.datamodel.broker.ProviderCredentialsManager;
import eu.specs.project.enforcement.broker.ChefServiceImpl;
import eu.specs.project.enforcement.contextlistener.ContextListener;
import eu.specs.project.enforcement.provisioning.Provisioner;

public class Main {
	private static ChefServiceImpl chefService;
	private static String databagId = "a7452816dbd74eab928fe109b9516056";
	private static String plan;


	public static void main(String[] args) {
		
		BufferedReader reader = null;
		StringBuilder  stringBuilder = new StringBuilder();
		try{
			File file = new File("./resources/implementation_plan.txt");  
		    reader = new BufferedReader( new FileReader (file));
		    String         line = null;
		    
		    String         ls = System.getProperty("line.separator");

		    while( ( line = reader.readLine() ) != null ) {
		        stringBuilder.append( line );
		        stringBuilder.append( ls );
		    }
		    reader.close();
		}catch(Exception e){
			e.printStackTrace();
		}


	    plan = stringBuilder.toString();
	    
		String username = "grimaldi";
		String tenant = "SPECS";	//project associated to the user
		String password = "grim16SPECS";
		String cloudAddress = "http://193.206.108.198:5000/v2.0";
		String groupName = "amgroup1";
		

		//provider credentials used to send request to the OpenStack API server  
		ProviderCredential provCred = new ProviderCredential(tenant+":"+username, password);
		ProviderCredentialsManager.add("provider", provCred);

		
		//get the credentials from the servlet context 
		//the context will be substituted with the credential manager
		ContextListener cc = new ContextListener();
		NodeCredential myCred = cc.brokerCredentials();
		System.out.println("le credenziali sono: "+(myCred==null ? "nulle":"non nulle"));
		System.out.println("public key: "+myCred.getPublickey());

		NodeCredential cred = new NodeCredential("specs.123456", 
				myCred.getPublickey(), 
				myCred.getPrivatekey());
		NodeCredentialsManager.add("def", cred);
		
		Provisioner provisioner = new Provisioner(plan, username, password, tenant, cloudAddress, groupName);
		

	}




	private static void uploadDatabag(){
		String databagId = UUID.randomUUID().toString().replace("-","");
		try {
			chefService.uploadDatabagItem("implementation_test",  databagId, "{\"plan_id\":\"1\",\"value\":\"30\"}");
		} catch (ResourceNotFoundException e) {
			e.printStackTrace();
		}
	}	
	
	
}
